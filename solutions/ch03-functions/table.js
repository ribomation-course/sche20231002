
let sum = function (n) {
    let result = 0;
    for (let k=1; k<=n; ++k) result += k;
    return result;
}


let prd = (n) => { 
    let result = 1;
    for (let k=1; k<=n; ++k) result *= k;
    return result;
}

function fib(n) { 
    if (n < 0) return 0;
    if (n === 0) return 0;
    if (n === 1) return 1;
    return fib(n-2) + fib(n-1);
}

function main(N) {
    for (let k = 1; k<=N; ++k) {
        console.log('%d\t%d\t%d\t%d', k, sum(k), fib(k), prd(k));
    }
}

main(+(process.argv[2] || 10));
