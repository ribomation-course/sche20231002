
const { readFileSync } = require('fs');
const data = JSON.parse(readFileSync('cars.json').toString())

class Car {
    id;
    producer;
    model;
    year;
    vin;
    city;
    constructor(id, producer, model, year, vin, city) {
        this.id = id;
        this.producer = producer;
        this.model = model;
        this.year = year;
        this.vin = vin;
        this.city = city;
    }
}

const cars = data
            .map(obj => new Car(obj.id, obj.producer, obj.model, obj.year, obj.vin, obj.city))
            .sort((a, b) => b.year - a.year)
            .slice(0, 10)

console.log(cars)
