
const todos = [];

class Todo {
    text = '';
    before = new Date();
    done = false;
    dom = undefined;

    constructor(text, date, done=false) {
        this.text = text;
        this.before = date;
        this.done = done
    }

    toggle() {
        thid.done = !this.done;
    }

    toDOM(idx) {
        const article = document.createElement('article');
        article.innerHTML = `
        <h5>
            <span>${todo.before}</span>
            <button>Done</button>
            <button>Remove</button>
        </h5>
        <p>${todo.text}</p>
        `;
        if (todo.done) article.classList.add('done');
        return article;
    }
}

function add(text, date, done=false) {
    const obj = new Todo(text, date, done);
    todos.push(obj);
}

function populate() {
    const tasks = document.querySelector('#tasks');
    tasks.innerHTML = '';
    todos.forEach(todo => tasks.appendChild(todo.toDOM()));
}

function main() {
    add('Learn JS', '2023-10-02', true);
    add('Practice JS', '2023-10-03');
    add('Master JS', '2023-10-04');
    add('Write some nifty JS in Artvise', '2023-10-06');
    populate();
}

main();
