
function weekdayOf(date) {
    if (date === undefined || date ===  null) {
        date = new Date()
    }
    if (typeof date === 'string') {
        date = new Date(date)
    }
    return date.toLocaleDateString('sv-SE', {weekday:'long'})
}

console.log(weekdayOf(new Date()));
console.log(weekdayOf());
console.log(weekdayOf('2023-12-24'));
