
const todos = [];

class Todo {
    text = '';
    before = new Date();
    done = false;
    dom = undefined;

    constructor(text, date, done = false) {
        this.text = text;
        if (typeof date === 'string') {
            this.before = new Date(date);
        } else {
            this.before = date;
        }
        this.done = done
    }

    toggle() {
        this.done = !this.done;
        this.dom.classList.toggle('done');
        // if (this.done) {
        //     this.dom.classList.add('done');
        // } else {
        //     this.dom.classList.remove('done');
        // }
    }

    toDOM(idx) {
        const article = document.createElement('article');
        this.dom = article;

        article.innerHTML = `
            <h5>
                <span>${this.before.toLocaleDateString()}</span>
                <button class="done">Done</button>
                <button class="remove">Remove</button>
            </h5>
            <p>${this.text}</p>
        `;

        if (this.done) {
            article.classList.add('done');
        }

        article.querySelector('.done')
            .addEventListener('click', (ev) => {
                this.toggle();
            });

        article.querySelector('.remove')
            .addEventListener('click', (ev) => {
                console.log('REMOVE', idx);
                const evt = new CustomEvent('remove-todo', {
                    detail: {
                        index: idx,
                        node: this.dom
                    }
                });
                document.querySelector('#tasks').dispatchEvent(evt);
            });

        return article;
    }
}

function add(text, date, done = false) {
    const obj = new Todo(text, date, done);
    todos.push(obj);
}

function populate() {
    const tasks = document.querySelector('#tasks');
    tasks.innerHTML = '';
    let index = 0;
    todos.forEach(todo => tasks.appendChild(todo.toDOM(index++)));
}

function main() {
    // add('Learn JS', '2023-10-02', true);
    // add('Practice JS', '2023-10-03');
    // add('Master JS', '2023-10-04');
    // add('Write some nifty JS in Artvise', '2023-10-06');
    populate();

    document.querySelector('#tasks')
        .addEventListener('remove-todo', (ev) => {
            console.log(ev)
            todos.splice(ev.detail.index, 1);
            //populate();
            ev.target.removeChild(ev.detail.node);
        });

    document.querySelector('#saveBtn')
        .addEventListener('click', (ev) => {
            const whatEl = document.querySelector('#textFld');
            const what = whatEl.value;
            const when = document.querySelector('#beforeFld').value;
            //$('#textFld').val() //jQuery
            
            if (what.trim().length > 0 && when.trim().length > 0) {
                console.log(what, when);
                add(what, when);
                populate();
                whatEl.value = '';
                document.querySelector('#beforeFld').value = '';
            }
        });
}

main();
