function Product(name, price, outOfStock) {
    this.name = name;
    this.price = price;
    this.outOfStock=outOfStock;
}

let obj1 = new Product('Apple', 12.4, false);
let obj2 = new Product('Banana', 34.5, true);
let obj3 = new Product('Coco Nut', 67.8, false);
console.log(obj1);
console.log(obj2);

[obj1, obj2, obj3].forEach((obj) => {
    Object.entries(obj).forEach(([name,value]) => {
        console.log('%s = %o', name, value);
    });
    console.log('----');
});

