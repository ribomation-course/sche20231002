const { log } = require('console');
const {readFileSync} = require('fs');

const filename = 'musketeers.txt'

let freqs = readFileSync(filename)
    .toString()
    .split(/[^a-z]+/i)
    .filter(w => w.length >= 5)
    .map((w) => w.toLowerCase())
    .reduce((freqs, w) => {
        const count = +freqs[w] || 0;
        freqs[w] = count + 1;
        return freqs;
    }, {});

//console.log(freqs);

let byFreqDesc = (lhs, rhs) => {
    const cntLeft = lhs[1];
    const cntRight = rhs[1];
    return cntRight - cntLeft;
};
let list = Object.entries(freqs)
      .sort(byFreqDesc)
      .slice(0, 25);

console.log(list)
