
//name, price, outOfStock

let obj1 = {name:'Apple', price: 12.3, outOfStock:false}
let obj2 = {name:'Banana', price: 5.5, outOfStock:true}
console.log(obj1);
console.log(obj2);

Object.entries(obj1).forEach(([name,value]) => {
    console.log('%s = %o', name, value);
});
