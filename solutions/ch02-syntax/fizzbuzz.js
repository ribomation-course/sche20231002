const N = +(process.argv[2] || 20);

for (let k = 1; k <= N; ++k) {
    if (k % 3 === 0 && k % 5 === 0) {
        console.log('FizzBuzz');
    } else if (k % 3 === 0) {
        console.log('Fizz');
    } else if (k % 5 === 0 ) {
        console.log('Buzz');
    } else {
        console.log(k);
    }
}
