const {readFileSync} = require('fs');

const filename = 'musketeers.txt'
const content  = readFileSync(filename).toString();
const words    = content.split(/[^a-z]+/i);

const bySize = (lhs, rhs) => rhs.length - lhs.length;
let result = words
            //.filter((str, index, lst) => lst.indexOf(str) === index)
            .sort(bySize)
            //.slice(0, 10);

result = [... new Set(result)].slice(0, 10);

console.log(result);
