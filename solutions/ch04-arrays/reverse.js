
function reverse_sentence(txt) {
    return txt.split(' ').reverse().join(' ');
}

let txt = 'hej hipp tjolla hopp';
console.log('%s -> %s', txt, reverse_sentence(txt));

