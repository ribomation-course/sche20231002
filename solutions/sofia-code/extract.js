
//[Marker](57.1831605, 14.0478214)[/Marker][Address]KYRKTORGET 1, 331 31 Värnamo, Sverige[/Address]

let txt = '[Marker](57.1831605, 14.0478214)[/Marker][Address]KYRKTORGET 1, 331 31 Värnamo, Sverige[/Address]'

txt = txt.replace('[Marker](', '')
txt = txt.replace(')[/Marker][Address]', ';')
txt = txt.replace('[/Address]', '')

let [coord, address] = txt.split(';')
let [lng, lat] = coord.split(', ')

console.log(lng, lat);
console.log(address)

//KYRKTORGET 1, 331 31 Värnamo, Sverige
let match = [...address.matchAll(/.*, \d\d\d \d\d ([a-zA-ZåäöÅÄÖ]+), .*/g)]
console.log(match)

let [[, city]] = match;
console.log(city)
