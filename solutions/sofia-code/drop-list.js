
const events = [
    { date:'2022-10-26', value: 1 },
    { date:'2022-11-23', value: 2 },
    { date:'2022-12-20', value: 3 },
    { date:'2023-01-18', value: 4 },
    { date:'2023-02-14', value: 5 },
    { date:'2023-04-12', value: 7 },
    { date:'2023-05-10', value: 8 },
];

function datePlus(date, days) {
    const DAY_MS = 24 * 3600 * 1000;
    const ts     = date.getTime() + days * DAY_MS;
    return new Date(ts);
}

const widget = '8fe0fbc7-46c0-4a7b-8e2e-063cb2fe41cb';
const today = new Date();
const extraDays = 2;
events.forEach(event => {
    if (today > datePlus(new Date(event.date), extraDays)) {
        $(`#${widget} option[value=${event.value}]`).hide();
    }
});

//------------
document.querySelectorAll(`#${widget} option`)
        .forEach(opt => {
           const val =  opt.value;
           const date = $(opt).text();
           if (today > new Date(date)) {
              $(`#${widget} option[value=${val}]`).hide();
           }
        })
