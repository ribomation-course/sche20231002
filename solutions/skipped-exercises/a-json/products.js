const {writeFileSync, readFileSync} = require('fs');

let products = [
    {name: 'Apple', price: 1},
    {name: 'Banana', price: 5},
    {name: 'Coco Nut', price: 3},
    {name: 'Date Plum', price: 4},
]

let str = JSON.stringify(products,null,3)
writeFileSync('products.json', str, 'utf8');

let data = readFileSync('products.json', 'utf8').toString();
let products2 = JSON.parse(data);
console.log(products2);
