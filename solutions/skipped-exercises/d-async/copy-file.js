const { readFile, writeFile } = require('fs')
const { basename } = require('path')

function readFilePromise(filename) {
    const body = (success, failure) => {
        readFile(filename, 'utf8', (err, payload) => {
            if (err) failure(err)
            else success(payload)
        })
    }
    return new Promise(body)
}

function writeFilePromise(filename, payload) {
    const body = (success, failure) => {
        writeFile(filename, payload, 'utf8', (err) => {
            if (err) failure(err)
            else success()
        })
    }
    return new Promise(body)
}

async function copyFile(infile, outfile) {
    try {
        const txt = await readFilePromise(infile)
        await writeFilePromise(outfile, txt.toUpperCase())
        console.log('written %d bytes to %s', txt.length, outfile);
    } catch (err) {
        console.error('copyFile failed: %s', err.message);
    }
}

const infile = process.argv[2] || __filename
const outfile = 'uppercase-of_' + basename(infile) + '.txt'
copyFile(infile, outfile)

