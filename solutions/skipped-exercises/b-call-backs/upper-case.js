const {readFile, writeFile} = require('fs');

const filename = './clocks.js'
const filename2 = 'upper-case-result.txt'

readFile(filename, 'utf8', (err, data) => {
    if (err) {
        console.error(err)
    } else {
        const content = data.toUpperCase()
        writeFile(filename2, content, 'utf8', (err) => {
            console.log('written', filename2);
        })
    }    
})

