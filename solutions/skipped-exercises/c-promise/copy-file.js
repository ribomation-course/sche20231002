const { readFile, writeFile } = require('fs')
const { basename } = require('path')

function readFilePromise(filename) {
    const body = (success, failure) => {
        readFile(filename, 'utf8', (err, payload) => {
            if (err) failure(err)
            else success(payload)
        })
    }
    return new Promise(body)
}

function writeFilePromise(filename, payload) {
    const body = (success, failure) => {
        writeFile(filename, payload, 'utf8', (err) => {
            if (err) failure(err)
            else success()
        })
    }
    return new Promise(body)
}

const infile = process.argv[2] || __filename
const outfile = 'uppercase-of_' + basename(infile) + '.txt'

readFilePromise(infile)
    .then(txt => {
        return writeFilePromise(outfile, txt.toUpperCase())
    })
    .then(() => {
        console.log('written %s', outfile);
    })
    .catch(err => {
        console.error('*** %s', err.message);
    })
