# JavaScript Basics 
### 2 &ndash; 4 October 2023

Welcome to this course that will get you up to speed with JavaScript programming.

# Links
* [Installation instructions](./installation-instructions.md)
* [Course Details](https://www.ribomation.se/programmerings-kurser/javascript/javascript/)



Course GIT Repo
====
It's recommended that you keep the git repo and your solutions separated. 
Create a dedicated directory for this course and a subdirectory for 
each chapter. Get the course repo initially by a `git clone` operation

    mkdir -p ~/javascript-course/my-solutions
    cd ~/javascript-course
    git clone <git HTTPS clone link> gitlab

![Git Clone](img/git-url.png)

During the course, solutions will be push:ed to this repo, and you can get these by
a `git pull` operation

    cd ~/javascript-course/gitlab
    git pull


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
