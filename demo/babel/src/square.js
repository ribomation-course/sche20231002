export function fancySquare(numbers) {
    if (!numbers) return undefined
    if (!Array.isArray(numbers)) return undefined
    if (numbers.length === 0) return []

    let [head, ...tail] = numbers
    return [head * head, ...fancySquare(tail)]
}


