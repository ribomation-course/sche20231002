let sumImpl = n => n * (n + 1) / 2

export function sum(n) {
    if (n <= 1) return 1
    return sumImpl(n)
}


