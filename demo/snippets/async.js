function fakeLoad(ix) {
    return new Promise((ok, err) => {
        setTimeout(() => {
            const values = ['anna', 'berit', 'carin', 'doris']
            const result = values[ix % values.length]
            ok(result)
        }, 500)
    })
}

let now = () => new Date().toISOString().slice(11,23)
let loop = async _ => {
    console.log(now(), '[loop] enter');
    for (let k=0; k<10; ++k) {
        const name = await fakeLoad(k)
        console.log(now(), '[loop] name:', name);
    }
    console.log(now(), '[loop] exit');
}

loop()


