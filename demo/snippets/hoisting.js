console.log('typeof one: %o', typeof one);
console.log('typeof two: %o', typeof two);

console.log('value of one: %s', one);
console.log('value of two: %s', two);

console.log('invocation of one: %o', one(5));
console.log('invocation of two: %o', two(5));

function one(n) {
    return n * 10
}

let two = function(n) {
    return n * 20
}


