function main() {
    const silly = 42
    console.log('[1] %o', silly)
    silly = 10
    console.log('[2] %o', silly)
}

main();
