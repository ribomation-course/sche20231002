let name = 'Justin Time'
let age = 52
let city = 'Stockholm'

let payload1 = {name:name, age:age, city:city}
console.log('payload1: %o', payload1)

let payload2 = {name, age, city}
console.log('payload2: %o', payload2)

let params = {id:12345, offset:3}
let payload3 = {name, age, city, ...params}
console.log('payload3: %o', payload3)

