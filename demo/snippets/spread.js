let arr1 = [10,20,30]
let arr2 = ['anna', 'berit']
let str = 'ABC'
let res1 = [arr1, arr2, str]
let res2 = [...arr1, ...arr2, ...str]
console.log('nested  :', res1)
console.log('injected:', res2)

let args = [2020, 10, 15, 11, 45]
let date = new Date(...args)
console.log('date    : %o', date)

let defs = {widht:10, height:25, color:'grey', style:'solid'}
let config = {...defs, color:'blue'}
console.log('config  : %o', config)


