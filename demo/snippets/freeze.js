const obj = { name: 'Nisse', age: 42 }
console.log('[1] obj: %o', obj)

obj.name = 'Anna'; obj.age += 10
console.log('[2] obj: %o', obj)

try {
    obj = { name: 'Berra', age: 33 }
} catch (err) {
    console.log('*** ' + err.message)
}

const obj2 = Object.freeze(obj)
console.log('[a] obj2: %o', obj2)

obj.name = 'Carin'; obj.age += 10
console.log('[b] obj2: %o', obj2)

