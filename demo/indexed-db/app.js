
import persons from './persons.json'
//console.log('data: %o', persons);

if (!window.indexedDB) {
    window.alert("Your browser doesn't support a stable version of IndexedDB.")
}

let DB

function setup() {
    const idbRequest = indexedDB.open('personsDB', 1)

    idbRequest.onerror = ev => {
        console.error('failed: %o', ev);
    }

    idbRequest.onupgradeneeded = ev => {
        console.log('db need upgrade');
        const db = ev.target.result
        const objStore = db.createObjectStore('persons', { autoIncrement: true })
        console.log('store created');
        persons.forEach(p => { objStore.add(p) })
    }

    idbRequest.onsuccess = ev => {
        DB = idbRequest.result
        console.log('db opened');
        loadAll(DB)
    }
}

function loadAll(db) {
    const tx = db.transaction('persons')
    const store = tx.objectStore('persons')
    store.getAll().onsuccess = ev => {
        const data = ev.target.result
        console.log('fetched %d records', data.length);
        const tbody = document.querySelector('#persons')
        const props = ['first_name', 'last_name', 'gender', 'email']
        data.forEach(d => {
            const tr = document.createElement('tr')
            props.forEach(p => {
                const td = document.createElement('td')
                td.appendChild(document.createTextNode(d[p]))
                tr.appendChild(td)
            })
            tbody.appendChild(tr)
        })
        console.log('populating table done');
    }
}


setup()

